# AOS573 Final Project

### Contents
1. Directory called "Data". The folder contains 12 NETCDF files for one hour of AORC precip data for each of the River Forecasting Centers ("AORC_APCP_**RFC_1979020100.nc4"). It also contains a combined version of this ("CombinedData.nc"). Lastly, it contains the precipitation data for the 10 cities examined in the project sorted by year("AOSYYYY.npy"). 
2.  There are 2 python files ("1.Download_and_Combine_RFC_AORC.py", "Combine_Hourly_NCDF_to_Daily_NCDF.py") that I used to to download all of the AORC data, unzip it and combine it. I then extract the single grid points for the cities to get the precip data. 
3. "__init__.py","distr.py" are files for the lmoments package. Sadly the downloaded version from the internet has issues in some of the functions becasue it is so old so I had to tweak a line in these. 
4. "FinalProj.ipynb" are the main code to run. 
5. "README.md" and "AOS573Proj.yml" are readme and yml files. 

