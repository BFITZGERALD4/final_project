# This function downloads and unzips one month of AORC data for all RFCs. 
# Input 1: Date in YYYYMM format
# Input 2: Destination for downloaded data 

def DownloadAORC(Date, Destination):
        RFCArray=['ABRFC', 'CBRFC','CNRFC', 'LMRFC', 'MARFC', 'MBRFC', 'NCRFC', 'NERFC', 'NWRFC', 'OHRFC', 'SERFC', 'WGRFC'];
        for aa in RFCArray:
            RFC=aa;
            print('Downloading '+aa+' Data for date '+Date)
            URL = "https://hydrology.nws.noaa.gov/aorc-historic/AORC_"+RFC+ "_4km/"+RFC+"_precip_partition/AORC_APCP_4KM_"+RFC+"_"+Date+".zip";
            FileName="AORC_APCP_4KM_"+RFC+"_"+Date+".zip";
            response = request.urlretrieve(URL, "AORCZips/"+FileName)
            with zipfile.ZipFile("AORCZips/"+FileName, 'r') as zip_ref:
                zip_ref.extractall('Months/'+Destination)
            

# This function adds an hour to a string by converting to datetime, adding an hour and then converting back to string.
# Input: Date in YYYYMMDDHH
# Output: Date in YYYYMMDDHH

def addHour(String):
    d1=datetime.datetime.strptime(String, "%Y%m%d%H")
    dd1=datetime.timedelta(hours=1);
    d2=datetime.datetime.strftime(d1+dd1, "%Y%m%d%H")
    return d2


# This function combines and saves the netcdfs for a certain time for all RFCs.
# Input 1: Date in YYYYMMDDHH format
# Input 2: Destination for combined data

def combineAORC(Time, Destination):
    RFCArray=['CBRFC','CNRFC', 'LMRFC', 'MARFC', 'MBRFC', 'NCRFC', 'NERFC', 'NWRFC', 'OHRFC', 'SERFC', 'WGRFC'];
    Year=Time[0:4]
    Month=Time[4:6]
    Day=Time[6:8]
    Hour=Time[8:10]
    
    d1=xarray.open_dataset('Months/'+Year+Month+'/AORC_APCP_ABRFC_'+Year+Month+Day+Hour+'.nc4')
    
    for aa in RFCArray:         
        d2=xarray.open_dataset('Months/'+Year+Month+'/AORC_APCP_'+aa+'_'+Year+Month+Day+Hour+'.nc4')
        ds= xarray.merge([d1,d2])    
        d1=ds 
        
    ds.to_netcdf(Destination+'/AORC_APCP.'+Year+'.'+Month+'.'+Day+'.'+Hour+'.nc')
       

# Program

# Set up 

from urllib import request
import shutil
import ssl
import zipfile
import xarray
import netCDF4 as nc
import numpy as np
import os
import datetime
import dateutil
ssl._create_default_https_context = ssl._create_unverified_context

# User defined values

# Dates valid are 197902 to 202207

StartDateStr='2021123100'
EndDateStr='2021123123'
FinalDestination='D:/AORC/Combined Data'
#'C:/Users/benfi/AORC Data/Combined Data'

# Convert Start and End strings into datetime and cut off day and hour so string format is only YYYYMM. Then convert back to date time so format is YYYY/MM/01 00:00:00

StartDT=datetime.datetime.strptime(StartDateStr, "%Y%m%d%H")
StartStrM=datetime.datetime.strftime(StartDT, "%Y%m")
StartDTM=datetime.datetime.strptime(StartStrM, "%Y%m")

EndDT=datetime.datetime.strptime(EndDateStr, "%Y%m%d%H")
EndStrM=datetime.datetime.strftime(EndDT, "%Y%m")
EndDTM=datetime.datetime.strptime(EndStrM, "%Y%m")

# Initialize while loop with time equal to our starting month's first day, then add month until loop is at first day of final month.
# Code set up to open by first day of month because files are zipped 

#os.mkdir("AORCZips")

DlDTM=StartDTM
while DlDTM<=EndDTM:
    DlMString=datetime.datetime.strftime(DlDTM, "%Y%m")
    DownloadAORC(DlMString, DlMString)
    DlDTM=DlDTM+dateutil.relativedelta.relativedelta(months=+1)

#shutil.rmtree("AORCZips")

print('Done with download and extraction.')
# Combine by hour

CDT=StartDT
while CDT<=EndDT:
    Str=datetime.datetime.strftime(CDT, "%Y%m%d%H")
    combineAORC(Str,FinalDestination)
    CDT=datetime.datetime.strptime(addHour(Str), "%Y%m%d%H")
    print(Str+' Data Completed')