import xarray
import datetime
import dateutil

def hour2day(Start,End,path1,path2, inname,outname):
    # Cut off hours, tbh probs an easier way to do this... 

    StartDT=datetime.datetime.strptime(Start, "%Y%m%d%H")
    StartStrD=datetime.datetime.strftime(StartDT, "%Y%m%d")
    StartDTD=datetime.datetime.strptime(StartStrD, "%Y%m%d")

    EndDT=datetime.datetime.strptime(End, "%Y%m%d%H")
    EndStrD=datetime.datetime.strftime(EndDT, "%Y%m%d")
    EndDTD=datetime.datetime.strptime(EndStrD, "%Y%m%d")


    # Initialize date variable for iteration
    DlDTD=StartDTD

    # Combine 24 hourly netcdfs for each day into a single daily netcdf with 24 time steps. Iterates until day is at the specified end date. 
      
    while DlDTD<=EndDTD:
        DlDString=datetime.datetime.strftime(DlDTD, "%Y%m%d")
        year=DlDString[0:4]
        month=DlDString[4:6]
        day=DlDString[6:8]
        Path=path1+inname[0]+year+'.'+month+'.'+day+'.*'+inname[1]
        d1=xarray.open_mfdataset(Path)
        d1.to_netcdf(path2+outname[0]+year+month+day+outname[1])
        DlDTD=DlDTD+dateutil.relativedelta.relativedelta(days=+1)
        print('Done with ', DlDString)





# Dates for combining data and destination
 
StartDateStr='1979020100'
EndDateStr='1979123123'
InputDestination='D:/AORC/Combined Data/'
FinalDestination='D:/AORC/AORCDailyFiles/'
fileform=['AORC_APCP.','.nc']
outputform=['AORC.','.precip.nc']


hour2day(StartDateStr, EndDateStr, InputDestination, FinalDestination, fileform, outputform)










